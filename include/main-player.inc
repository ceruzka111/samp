/*
 *
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined MAIN_PL
#else
#define MAIN_PL
/* OBSAH SCRIPTU */

    #include "../include/modules/translation.inc"

    // PRESSED(keys)
    #define PRESSED(%0) \
        (((newkeys & (%0)) == (%0)) && ((oldkeys & (%0)) != (%0)))

    // RELEASED(keys)
    #define RELEASED(%0) \
        (((newkeys & (%0)) != (%0)) && ((oldkeys & (%0)) == (%0)))

    // PRESSING(keyVariable, keys)
    #define PRESSING(%0,%1) \
        (%0 & (%1))

    /*
     * Includy modulov
     */
    #include "../include/modules/chat.inc"
    #include "../include/modules/commands.inc"
    #include "../include/colors.inc"
    #include "../include/modules/register.inc"

    /*
     * Ked sa pripoji
     */
    public OnPlayerConnect(playerid) {
        SetPlayerColor(playerid, 0x999999FF);
        Connect(playerid);
        return true;
    }

    /*
     * Ked sa hrac odpoji
     */
    public OnPlayerDisconnect(playerid, reason) {
        Disconnect(playerid, reason);
        return true;
    }

    /*
     * Ked si hrac vybera postavu
     */
    public OnPlayerRequestClass(playerid,classid) {
        SetPlayerPos(playerid, 681.70, -475.73, 16.33);
        SetPlayerFacingAngle(playerid, 180);
        SetPlayerCameraPos(playerid, 681.70, -489.57, 17.5);
        SetPlayerCameraLookAt(playerid, 681.70, -475.73, 16.33);

        if(amx_fork()) { // spolu s threaded spusti vlakno - sucastne vykonavanie
            threaded(sync_explicit) {
                tryLoad(playerid); // pokusi sa nacitat udaje o hracovi alebo vyvola registraciu
            }
        } // vo vetve else (ktora tu nie je) bezi hlavne vlakno
        return true;
    }

    /*
     * Ked sa hrac snazi spawnut pri vybere postavy - shift
     */
    public OnPlayerRequestSpawn(playerid) {
        return true;
    }

    /*
     * Ked sa hrac spawne
     */
    public OnPlayerSpawn(playerid) {
        SetPlayerPos(playerid, GetPData(playerid, "posx"), GetPData(playerid, "posy"), GetPData(playerid, "posz"));
        SetPlayerFacingAngle(playerid, GetPData(playerid, "posa"));
        /*new skill[10];
        for (new i = 0; i < 11; ++i) {
            format(skill, sizeof(skill), "skill%d", i);
            printf("%d", GetPData(playerid, skill));
            SetPlayerSkill(playerid, i, GetPData(playerid, skill));
        }*/
        return true;
    }

    /*
     * Ked zomrie
     */
    public OnPlayerDeath(playerid, killerid, reason) {
        return true;
    }

    /*
     * Ked hrac napise text do chatu
     */
    public OnPlayerText(playerid, text[]) {
        return Chat(playerid, text);
    }

    /*
     * Ked napise prikaz 
     * Nepouzivat! nahradene Pawn.CMD
     */
    public OnPlayerCommandText(playerid, cmdtext[]) {
        return false;
    }

    /*
     * Ked hrac napise prikaz a ten sa vykona/nevykona
     */
    public OnPlayerCommandPerformed(playerid, cmd[], params[], result, flags) {
        if (result == -1) {
            SCMLF(playerid, WRONG_CMD, cmd)
        }
        return true;
    } 

    /*
     * Ked stlaci enter pri vozidle
     */
    public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger) {
        return true;
    }

    /*
     * Ked stlaci enter vo vozidle
     */
    public OnPlayerExitVehicle(playerid, vehicleid) {
        return true;
    }

    /*
     * Ked sa zmenia hracove statistiky - tiez xz krát za sekundu
     */
    public OnPlayerUpdate(playerid) {
        return true;
    }

    /*
     * Ked stlaci tlacidlo v dialogu
     */
    public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
        if(amx_fork()) { // spolu s threaded spusti vlakno - sucastne vykonavanie
            threaded(sync_explicit) {
                RegisterDialog(playerid, dialogid, response, listitem, inputtext);
            }
        } // vo vetve else (ktora tu nie je) bezi hlavne vlakno
        return true;
    }

    /*
     * Ked hrac stlaci klavesu
     */
    public OnPlayerKeyStateChange(playerid, newkeys, oldkeys) {
        return true;
    }

    /*
     * Ked hrac spravi ikonu na mape (oznaci)
     */
    public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ) {
        return true;
    }

    /*
     * Ked hrac klikne na hraca v TABe
     */
    public OnPlayerClickPlayer(playerid, clickedplayerid, source) {
        return true;
    }

    /*
     * Ked vojde/odide z tuning garaze
     * interiorid == 0 -> odisiel
     */
    public OnEnterExitModShop(playerid, enterexit, interiorid) {
        return true;
    }

    /*
     * Ked sa hrac pripaja na server
     */
    public OnIncomingConnection(playerid, ip_address[], port) {
        return true;
    }

    /*
     * Ked klikne na PlayerTextDraw
     */
    public OnPlayerClickPlayerTextDraw(playerid, PlayerText:playertextid) {
        return true;
    }

    /*
     * Ked klikni na TextDraw
     */
    public OnPlayerClickTextDraw(playerid, Text:clickedid) {
        return true;
    }

    /*
     * Ked vojde do klasickeho CP
     */
    public OnPlayerEnterCheckpoint(playerid) {
        return true;
    }

    /*
     * Ked vojde do race CP
     */
    public OnPlayerEnterRaceCheckpoint(playerid) {
        return true;
    }

    /*
     * Ked opusti menu - menu vytvorene z modu
     */
    public OnPlayerExitedMenu(playerid) {
        return true;
    }

    /*
     * Ked hrac sposobi damage
     */
    public OnPlayerGiveDamage(playerid, damagedid, Float:amount, weaponid, bodypart) {
        return true;
    }

    /*
     * Ked hrac sposobi damage actorovi
     */
    public OnPlayerGiveDamageActor(playerid, damaged_actorid, Float: amount, weaponid, bodypart) {
        if (bodypart == 9) {
            amount *= 2.0;
        }
        printf("%d, %d, %f, %d, %d", playerid, damaged_actorid, amount, weaponid, bodypart);
        return true;
    }

    /*
     * Ked hrac zmeni interier
     */
    public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid) {
        return true;
    }

    /*
     * Ked hrac opusti CP
     */
    public OnPlayerLeaveCheckpoint(playerid) {
        return true;
    }

    /*
     * Ked hrac opusti race CP
     */
    public OnPlayerLeaveRaceCheckpoint(playerid) {
        return true;
    }

    /*
     * Ked hrac vstupi na pickup
     */
    public OnPlayerPickUpPickup(playerid, pickupid) {
        return true;
    }

    /*
     * Ked hrac zvoli polozku v menu
     */
    public OnPlayerSelectedMenuRow(playerid, row) {
        return true;
    }

    /*
     * Ked hrac zmeni stav - opusti vozidlo, ...
     */
    public OnPlayerStateChange(playerid, newstate, oldstate) {
        return true;
    }

    /*
     * Ked hrac dostane damage
     */
    public OnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid, bodypart) {
        return true;
    }

    /*
     * Ked hrac vystreli
     */
    public OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ) {
        return true;
    }

/* KONIEC SCRIPTU */
#endif