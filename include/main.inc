/*
 * Hlavny script celeho GM
 * Obsahuje vstupny bod serveru a jeho dolezite sucasti
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined MAIN
#else
#define MAIN
/* OBSAH SCRIPTU */

    #define GAMEMODE "Fort deffenders" // nazov GM

    #include "../include/modules/translation.inc"
    #include "../include/fSystem.inc"
    #include "../include/modules/npc.inc"

    /*
     * AntiDeAMX - zabrani konvertovaniu AMX na PWN - kedze sa jedna o verjny mod, nema to nejak vyznam
     */
    AntiDeAMX()
    {
        new a[][] =
        {
            "Unarmed (Fist)",
            "Brass K"
        };
        #pragma unused a
    }

    /* Vstupny bod programu
     * Spravidla vypise text do konzoly
     */
    main()
    {
        print("\n");
        print("================================================");
        print("==                                            ==");
        print("==            ESTE NEVIEM GAMEMOD             ==");
        print("==                    by                      ==");
        print("==            Jiyory a.k.a Quiter             ==");
        print("==                                            ==");
        print("================================================");
        print("\n");
    }

    new act;

    /*
     * Vola sa pri spusteni serveru, obsahuje nastavenia a spravidla aj vytvorenie objektov, vozidiel, ...
     */
    public OnGameModeInit() {
        AntiDeAMX(); // zabrani konvertovaniu na PWN
        SetGameModeText(GAMEMODE); // Nastavi nazov modu

        fSystemInit(); // zinicializuje suborovy system
        NPCInit();

        AddPlayerClass(0, 1958.3783, 1343.1572, 15.3746, 269.1425, 0, 0, 0, 0, 0, 0); // Prida postavu na vyber hracov

        /*new data[100];
        new count = 100000000;
        for (new i = 0; i < count; ++i) {
            ++data[random(100)];
        }
        for (new i = 0; i < 100; ++i) {
            printf("%d = %f", i, data[i] / float(count));
        }*/

        act = CreateActor(0, 649.7, -554.34, 16.27, 0.0); // CJ
        SetActorInvulnerable(act, 0);
        //Následne som použil cyklus, v ktorom som kontroloval či má dané vybrané ID splnené podmienky (nie je NPC, nejedná sa o hráča, ktorý prácu spustil)
        
        return true;
    }

    CMD:test(playerid, params[]) {
        ApplyActorAnimation(act, "RIOT", "RIOT_PUNCHES", 4.1, 1, 0, 0, 0, 0); // Pay anim
        return true;
    }

    /*
     * Vola sa pri ukonceni serveru (napr. prikazom gmx), spravidla uklada informacie o hracoch
     * POZOR! Pri zatvoreni konzoly (a pravdepodobne aj pri pade serveru) sa tato cas kodu nevola!
     */
    public OnGameModeExit() {
        fSystemDestroy(); // ukonci file system
        return true;
    }

    /*
     * Vykona sa, ak hrac pouzije /rcon COMMAND
     * Obvykle sa blokuje aby sa predislo zneuzitiu
     */
    public OnRconCommand(cmd[]) {
        return true;
    }

    /*
     * Vykona sa pri pokuse o prihlasenie pomocou /rcon login PASSWORD
     */
    public OnRconLoginAttempt(ip[], password[], success) {
        return true;
    }

    /*
     * Ked sa skonci nahravka (NPC)
     */
    /*public OnRecordingPlaybackEnd() {
        return true;
    }*/

    /*
        Pri vlaknach sa nedaju nastavovat premenne, avsak dokaze sa zdielat pamat mapy
        Na zmenu pouzit map_set(m, key, val);

        Príklad pouzitia threadov
        new Map:m = map_new();
        map_add(m, 1, 5);
        map_add(m, 2, false);
        map_add(m, 3, 0);

        if (amx_fork())
        {
            threaded(sync_explicit)
            {
                //thread_sleep(1000);
                for (new i = 0; i < 100; ++i) {
                    printf("slow %d", i);
                }
                map_add(m, 4, 7);
                map_add(m, 5, 7);
                map_add(m, 6, 7);
                map_add(m, 7, 7);
            }
        } else {
            threaded(sync_explicit)
            {
                //thread_sleep(1000);
                for (new i = 0; i < 100; ++i) {
                    printf("fast %d", i);
                }
                for_map(i : m)
                //for(new Iter:i = map_iter(m); iter_inside(i); iter_move_next(i))
                {
                    new c, d;
                    iter_get_key_safe(i, c);
                    iter_get_value_safe(i, d);
                    printf("%d -> %d", c, d);
                }
                //print("Thread end");
            }
            //print("Fork end 2");
        }
    */

/* KONIEC SCRIPTU */
#endif