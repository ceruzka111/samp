/*
 * Modul obsahuje funkcie ktore by mohli ulahcit pracu s funkciami
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined UTILS
#else
#define UTILS
#pragma tabsize 0
/* OBSAH SCRIPTU */

    /*
     * Posle spravu hracovi
     * Osetrenie voci dlhej sprave - chat dokaze zobrazit iba 144 znakov (vratane farieb)
     * Odporucam pouzit SCML - prelozene spravy
     */
    stock _SCM(const playerid, const text[]) {
        if (strlen(text) > 144) {
            print("Tento string bol dlhy:");
            print(text);
            new txt[145]; // maximalna dlzka zobrazenej spravy
            strmid(txt, text, 0, 145);
            return SendClientMessage(playerid, -1, txt);
        } else {
            return SendClientMessage(playerid, -1, text);
        }
    }

    /*
     * Posle spravu vsetkym
     * Osetrenie voci dlhej sprave - chat dokaze zobrazit iba 144 znakov (vratane farieb)
     * Odporucam pouzit SCMLA - prelozene spravy
     */
    stock _SCMTA(const text[]) {
        if (strlen(text) > 144) {
            print("Tento string bol dlhy:");
            print(text);
            new txt[145]; // maximalna dlzka zobrazenej spravy
            strmid(txt, text, 0, 145);
            return SendClientMessageToAll(-1, txt);
        } else {
            return SendClientMessageToAll(-1, text);
        }
    }

    /*
     * Posle formatovanu spravu danemu hracovi
     * Na konci nepouzivat ";" - inak vyskoci error
     * playerid - hrac
     * string - sprava
     * pamas - lubovolny pocet parametrov ktore chcem vlozit do spravy
     * Pouzitie: SCMF(playerid, "Sprava ktoru %s naformatovat", "chcem") // bez ;
     */
    #define _SCMF(%1,%2,%3) {new _str[145];format(_str,sizeof(_str),%2,%3);_SCM(%1,_str);}

    /*
     * Posle formatovanu spravu vsetkym hracom
     * Na konci nepouzivat ";" - inak vyskoci error
     * string - sprava
     * pamas - lubovolny pocet parametrov ktore chcem vlozit do spravy
     * Pouzitie: SCMFA("Sprava ktoru %s naformatovat", "chcem") // bez ;
     */
    #define _SCMFA(%2,%3) {new _str[145];format(_str,sizeof(_str),%2,%3);_SCMTA(_str);}

    /*
     * Vrati meno hraca
     */
    stock Name(const playerid) {
        new n[25];
        GetPlayerName(playerid, n, 25);
        return n;
    }

    /*
     * Vrati ip adresu hraca
     */
    stock Ip(const playerid) {
        new n[16];
        GetPlayerIp(playerid, n, 25);
        return n;
    }

    /*
     *
     */
    stock GiveCash(const playerid, const cash) {
        SetPVarInt(playerid, "cash", GetPVarInt(playerid, "cash") + cash);
        GivePlayerMoney(playerid, GetPVarInt(playerid, "cash") - GetPlayerMoney(playerid))
        return GetPVarInt(playerid, "cash");
    }

    stock GetCash(const playerid) {
        return GetPVarInt(playerid, "cash");
    }

    stock SetPlayerSkill(const playerid, const skill, const level) {
        SetPlayerSkillLevel(playerid, skill, level);
        new str[10];
        format(str, sizeof(str), "_skill%d", skill);
        SetPVarInt(playerid, str, level);
        return level;
    }

    stock GetPlayerSkill(const playerid, const skill) {
        new str[10];
        format(str, sizeof(str), "_skill%d", skill);
        return GetPVarInt(playerid, str);
    }

/* KONIEC SCRIPTU */
#endif