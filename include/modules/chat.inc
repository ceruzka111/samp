/*
 * Modul obsahuje spravovanie chatu, taktiez zakazane slova
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined CHAT
#else
#define CHAT
/* OBSAH SCRIPTU */

    #define BANNED_WORDS_LENGTH 29
    new const bannedWords[][] = {
        "debil", "de*il", "d*bil", "d e b i l", "de bil", "deb il",
        "idiot", "id*ot", "id iot", "i d i o t", "idi ot",
        "kokot", "ko*ot", "ko kot", "k o k o t", "kok ot",
        "piča", "pi ča", "pi*a", "pi*ča", "pič a",
        "kretén", "kreten", "kre ten", "kre tén", "kre*en", "kre*én", "kre*tén", "kre*ten" //29
    };

    forward Chat(const playerid, const text[]);
    public Chat(const playerid, const text[]) {
        new msg[144];
        format(msg, sizeof(msg), "%s (%d): {FFFFFF} %s", Name(playerid), playerid, text);
        /* Algoritmus pre "vymazanie" zablokovanych slov */
        new pos;
        for (new i = 0; i < BANNED_WORDS_LENGTH; ++i) {
            pos = strfind(msg, bannedWords[i], true);
            while (pos != -1) {
                for (new j = 0; j < strlen(bannedWords[i]); j += 2) {
                    msg[pos + j] = '*';
                }
                pos = strfind(msg, bannedWords[i], true);
            }
        }
        SendClientMessageToAll(GetPlayerColor(playerid), msg);
        return false;
    }

/* KONIEC SCRIPTU */
#endif