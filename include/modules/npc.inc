/*
 * Kontrolovatelne NPC
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined NPC
#else
#define NPC
/* OBSAH SCRIPTU */

    #if defined MAX_STATIC_NPC
    #else
    #define MAX_STATIC_NPC 500
    #endif

    new _NPCID[MAX_STATIC_NPC];
    new _NPCOBJ[MAX_STATIC_NPC];
    new _NPCTIMER[MAX_STATIC_NPC];

    stock NPCInit() {
        for (new i = 0; i < MAX_STATIC_NPC; ++i) {
            _NPCID[i] = -1;
        }
    }

    stock AddStaticNPC(model, Float:x, Float:y, Float:z, Float:a) {
        for (new i = 0; i < MAX_STATIC_NPC; ++i) {
            if (_NPCID[i] == -1) {
                _NPCID[i] = CreateActor(model, x, y, z, a);
                _NPCOBJ[i] = CreateObject(1448, x, y, z, 0.0, 0.0, 0.0);
                return i;
            }
        }
        return -1;
    }

    stock RemoveStaticNPC(npc) {
        DestroyActor(_NPCID[npc]);
        DestroyObject(_NPCOBJ[npc]);
        _NPCID[npc] = -1;
    }

    stock SetStaticNPCPos(npc, Float:x, Float:y, Float:z) {
        SetActorPos(_NPCID[npc], x, y, z);
    }

    stock StaticNPCMoveTo(npc, Float:x, Float:y, Float:z) {
        ApplyActorAnimation(_NPCID[npc], "ped", "walk_fatold", 4.1, 1, 0, 0, 0, 0);
        MoveObject(_NPCOBJ[npc], x, y, z, 0.8);
        if (_NPCTIMER[npc] > 0) {
            KillTimer(_NPCTIMER[npc]);
        }
        _NPCTIMER[npc] = SetTimerEx("_moveStaticNPC", 1050, true, "i", npc);
    }

    forward _moveStaticNPC(npc);
    public _moveStaticNPC(npc) {
        ClearActorAnimations(_NPCID[npc]);
        ApplyActorAnimation(_NPCID[npc], "ped", "walk_fatold", 4.1, 0, 0, 0, 0, 0);
        
        new Float:x, Float:y, Float:z;
        GetObjectPos(_NPCOBJ[npc], x, y, z);
        SetActorPos(_NPCID[npc], x, y, z);
    }

/* KONIEC SCRIPTU */
#endif