/*
 * GameMode by Jiyory
 * Struktura:
 *				../include	- obsahuje vsetky moduly
 *							/main.inc			- obsahuje callbacky pre GM
 *							/main-player.inc	- obsahuje callbacky pre hraca
 *							/main-vehicle.inc	- obsahuje callbacky pre vozidlá
 *							/main-object.inc	- obsahuje callbacky pre objekty
 *							/main-stream.inc	- obsahuje callbacky pre streamovanie
 */

/* SAMP INCLUDE */
#include <a_samp>

#define PP_SYNTAX           // povoluje vlakna
#pragma warning disable 239 // kvoli GetPVarInt
#pragma warning disable 217 // kvoli define
#pragma warning disable 214 // kvoli PawnPlus a threadingu
#include <PawnPlus>
#include <Pawn.CMD>
#include <sscanf2>
/* UTILS */
#include "../include/utils.inc"
#include "../include/fSystem.inc"
/* PRIMARNE FUNKCIE */
#include "../include/main.inc"
/* HRACSKE FUNKCIE */
#include "../include/main-player.inc"
/* VOZIDLOVE FUNKCIE */
#include "../include/main-vehicle.inc"
/* OBJEKTOVE FUNKCIE */
#include "../include/main-object.inc"
/* STREAMOVACIE FUNKCIE */
#include "../include/main-stream.inc"
